## **MINI_STORE PROJECT**
---

### **Technoloies Used**

- Enviroment requirements
    - python3 (3.8)
    - pip3
    - mongodb
- Tool
    - Visual Code (Pycharm)
    - Robo3t
    - Postman

### **Installation and set up**

<br>

#### Clone the source code

```
git clone https://gitlab.com/doanson/mini-store.git
```

### Install virtualenv

```
pip install virtualenv
```

### Create new environment

```
virtualenv my_env
```

### Activate enviroment (window)

```
.\my_env\Scripts\activate
```

### Change directory

```
cd mini_store
```

### Install the requirementt

```
pip install -r requirements.txt
```

### Start the app

```
python manage.py runserver
```

### **List of API**

1. User
    1. Register user
    2. Login user
    3. Update user
    4. Delete User
    5. Add role for user

2. Menu
    1. Create Menu
    2. Get all menu
    3. Get a menu detail
    4. Update menu

3. Dish
    1. Create dish
    2. Update dish
    3. Delete dish

4. Order
    1. Create order

5. Upload
    1. Upload image to server

### **Postman**

> Post man collection file link [collection link](https://www.dropbox.com/s/h18holmodts80jf/mini-store-Api.postman_collection?dl=0)

