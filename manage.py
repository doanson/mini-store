#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys
from bson.objectid import ObjectId
from mini_store.common.bycrypt import get_hashed_password
from mini_store.common.connect import connect_db

db = connect_db('127.0.0.1', 27017, 'mini-store')
db_user = db['user']

admin_user = db_user.find_one({"username": "admin", "active": True})
if admin_user:
    pass
else:
    db_user.insert_one({
        "_id": str(ObjectId()),
        "active": True,
        "username": "admin",
        "role": "admin",
        "first_name": "Doan",
        "last_name": "Son",
        "password": get_hashed_password("admin")

    })

def main():
    """Run administrative tasks."""
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'mini_store.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
