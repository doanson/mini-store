from django.contrib import admin
from django.urls import path
from django.views.decorators.csrf import csrf_exempt
from mini_store import views


urlpatterns = [
    # User
    path("api/v1/register", csrf_exempt(views.register), name="register"),
    path("api/v1/login", csrf_exempt(views.login), name="login"),
    path("api/v1/get-user-from-token", csrf_exempt(views.get_user_from_token), name="create_menu"),
    path("api/v1/user/update", csrf_exempt(views.update_user), name="update_user"),
    path("api/v1/user/delete", csrf_exempt(views.delete_user), name="delete_user"),

    # Dish
    path("api/v1/create-dish", csrf_exempt(views.create_dish), name="create_dish"),
    path("api/v1/update-dish", csrf_exempt(views.update_dish), name="update_dish"),
    path("api/v1/delete-dish", csrf_exempt(views.delete_dish), name="delete"),
    path("api/v1/get-dish", csrf_exempt(views.get_dish), name="get_dish"),
    path("api/v1/get-all-dish", csrf_exempt(views.get_all_dish), name="get_all_dish"),
    # Menu
    path("api/v1/create-menu", csrf_exempt(views.create_menu), name="create_menu"),
    path("api/v1/get-menu-detail", csrf_exempt(views.get_menu_details), name="get_menu_detail"),
    path("api/v1/get-all-menu", csrf_exempt(views.get_all_menu), name="create_menu"),
    path("api/v1/delete-menu", csrf_exempt(views.delete_menu), name="create_menu"),
    path("api/v1/update-menu", csrf_exempt(views.update_menu), name="create_menu"),

    # Upload image
    path("api/v1/dish/upload-image", csrf_exempt(views.upload_dish_image), name="upload-dish-image"),
    path("api/v1/menu/upload-image", csrf_exempt(views.upload_menu_image), name="upload-menu-image"),
    path("api/v1/order/upload-image", csrf_exempt(views.upload_order_image), name="upload-order-image"),
    # Order
    path("api/v1/delete-order", csrf_exempt(views.delete_order), name="delete-order"),
    path("api/v1/edit-order", csrf_exempt(views.edit_order), name="delete-order"),
    path("api/v1/get-all-order", csrf_exempt(views.get_all_order), name="get_all_order"),
    path("api/v1/create-order", csrf_exempt(views.create_order), name="create_order"),
    path("api/v1/get-order-detail", csrf_exempt(views.get_order_details), name="get_order_details"),
    path("api/v1/get-user-order", csrf_exempt(views.get_user_order), name="get_order_details"),
    # path("api/v1/menu/upload-image", csrf_exempt(views.upload_menu_image), name="upload-dish-image"),

    # Get all user
    path("api/v1/get-all-user", csrf_exempt(views.get_all_user), name="get_all_user"),
    path("api/v1/get-user-info", csrf_exempt(views.get_user_info), name="get_all_user"),
    path("api/v1/user/update-password", csrf_exempt(views.update_password), name="get_all_user"),


    # path("api/v1/create-category", csrf_exempt(views.create_category), name="create_category")
    # path('admin/', admin.site.urls),
]
