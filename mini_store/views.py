from mini_store.common.utils import handle_error, handle_success
from rest_framework import status
from rest_framework.decorators import api_view
import json

from mini_store.common.connect import connect_db

from mini_store.services import user
from mini_store.services import dish
from mini_store.services import menu
from mini_store.services import order
from mini_store.services import upload
from mini_store.common.token import validate_token, check_role_admin


# from mini_store.common.token import decode_token


@api_view(["POST"])
def login(request):
    try:
        request_body = json.loads(request.body)

        data = user.validate_user(request_body)
        return handle_success(data)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
def register(request):
    try:
        request_body = json.loads(request.body)
        result = user.handle_register(request_body)
        return handle_success(result)

    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
@check_role_admin
def create_dish(request):
    try:
        result = dish.handle_create_dish(request.body)
        return handle_success(result)

    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
@check_role_admin
def update_dish(request):
    try:
        result = dish.handle_update_dish(request.body)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
@check_role_admin
def delete_dish(request):
    try:
        id = request.body.get("id")
        result = dish.handle_delete_dish(id)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
@check_role_admin
def create_menu(request):
    try:
        result = menu.handle_create_menu(request.body)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
def get_menu_details(request):
    try:
        request_body = json.loads(request.body)
        id = request_body.get("id")

        data = menu.get_menu_details(id)
        return handle_success(data)

    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
# @check_role_admin
def create_order(request):
    try:
        data = order.handle_create_order(request.body)
        return handle_success(data)

    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
def get_order_details(request):
    try:
        request_body = json.loads(request.body)
        data = order.get_order_detail(request_body)
        return handle_success(data)

    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
def update_user(request):
    try:
        result = user.handle_update_user(request.body)
        return handle_success(result)

    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
@check_role_admin
def delete_user(request):
    try:
        request_body = json.loads(request.body)
        user_role = request.decoded_token.get("role")
        user_id = request.decoded_token.get("id")

        result = user.handle_delete_user(request_body, user_role, user_id)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['GET', 'POST'])
@validate_token
@check_role_admin
def upload_dish_image(request):
    try:
        file_obj = request.data.getlist('image')

        # result = user.upload_dish_image(file_obj, user_data, dish_id)
        result = upload.upload_image(file_obj)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['GET', 'POST'])
@validate_token
@check_role_admin
def upload_menu_image(request):
    try:
        file_obj = request.data.getlist('image')

        result = upload.upload_image(file_obj)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['GET', 'POST'])
@validate_token
@check_role_admin
def upload_order_image(request):
    try:
        file_obj = request.data.getlist('image')

        result = upload.upload_image(file_obj)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
def get_all_menu(request):
    try:
        request_body = json.loads(request.body)
        current_page = request_body.get("current_page", 1)
        record_per_page = request_body.get("record_per_page", 5)

        result = menu.get_all_menu(current_page, record_per_page)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
def get_user_from_token(request):
    try:
        decoded_token = request.body.get("user_info")
        user_id = decoded_token.get("_id")
        user_info = user.get_user_info(user_id)

        return handle_success(user_info)


    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
def get_dish(request):
    try:
        request_body = json.loads(request.body)
        id = request_body.get("id")
        result = dish.get_dish(id)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
def get_all_dish(request):
    try:
        request_body = json.loads(request.body)
        current_page = request_body.get("current_page", 1)
        record_per_page = request_body.get("record_per_page", 5)

        result = dish.get_all_dish(current_page, record_per_page)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
def delete_order(request):
    try:
        id = request.body.get("id")
        result = order.handle_delete_order(id)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
@check_role_admin
def get_all_order(request):
    try:
        user_id = request.body.get("id")
        current_page = request.body.get("current_page", 1)
        record_per_page = request.body.get("record_per_page", 5)

        result = order.get_all_order(current_page, record_per_page, user_id)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
def edit_order(request):
    try:
        id = request.body.get("id")

        result = order.handle_edit_order(id, request.body)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
@check_role_admin
def delete_menu(request):
    try:
        id = request.body.get("id")
        result = menu.handle_delete_menu(id)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['GET', 'POST'])
@validate_token
@check_role_admin
def update_menu(request):
    try:
        id = request.body.get("id")
        result = menu.update_menu(id, request.body)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
@check_role_admin
def get_all_user(request):
    try:
        id = request.body.get("id")
        current_page = request.body.get("current_page", 1)
        record_per_page = request.body.get("record_per_page", 5)
        result = user.get_all_user(current_page, record_per_page)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
def update_password(request):
    try:

        result = user.update_password(request.body)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
def get_user_order(request):
    try:
        current_page = request.body.get("current_page", 1)
        record_per_page = request.body.get("record_per_page", 5)
        user_id = request.body.get("user_id")

        result = order.get_all_order(current_page, record_per_page, user_id)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)


@api_view(['POST'])
@validate_token
@check_role_admin
def get_user_info(request):
    try:
        id = request.body.get("id")
        result = user.get_user_info(id)
        return handle_success(result)
    except Exception as e:
        return handle_error(e)
