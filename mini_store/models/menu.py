import datetime
from marshmallow import fields, Schema, EXCLUDE
from mini_store.common.utils import generate_date


class Menu(Schema):
    name = fields.Str(max_length=100)
    dish = fields.List(fields.Str())
    description = fields.Str()

    # Meta data
    created_date = fields.Str(default=generate_date(), missing=generate_date())
    updated_date = fields.Str(default=generate_date(), missing=generate_date())
    photos = fields.List(fields.Str, default=[])
    active = fields.Boolean(default=True, missing=True)

    class Meta:
        unknown = EXCLUDE
