from marshmallow import Schema, fields, EXCLUDE
import datetime
from mini_store.common.utils import generate_date


class Dish(Schema):
    name = fields.Str(required=False)
    description = fields.Str(required=False)
    price = fields.Float(required=False)

    # Meta data
    created_date = fields.Str(default=generate_date(), missing=generate_date())
    updated_date = fields.Str(default=generate_date(), missing=generate_date())
    photos = fields.List(fields.Str, default=[], missing=[])
    active = fields.Boolean(default=True, missing=True)

    class Meta:
        unknown = EXCLUDE
