from datetime import datetime
from marshmallow import fields, Schema, validate, EXCLUDE
from mini_store.common.utils import generate_date


class User(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)
    email = fields.Str(required=True)
    first_name = fields.Str(required=False)
    last_name = fields.Str(required=False)
    role = fields.Str(validate=validate.OneOf(["user"]), default="user", missing="user")
    height = fields.Number(required=False)
    weight = fields.Number(required=False)
    active = fields.Boolean(default=True, missing=True)

    # Meta data
    created_date = fields.Str(default=generate_date(), missing=generate_date())
    updated_date = fields.Str(default=generate_date(), missing=generate_date())

    class Meta:
        unknown = EXCLUDE

# signals.pre_save.connect(User.pre_save, sender=User)
