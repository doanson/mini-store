import datetime
from marshmallow import fields, Schema, EXCLUDE
import uuid
from mini_store.common.utils import generate_date


class Order(Schema):
    # order_id = fields.Str(missing=uuid.uuid4())
    dish = fields.List(fields.Dict())
    customer_id = fields.Str(required=True)
    name = fields.Str(required=False)
    # amount = fields.Int(required=False, missing=0, default=0)
    paid = fields.Boolean(missing=False, default=False)


    # Meta data
    order_date = fields.Str(default=datetime.datetime.utcnow)
    created_date = fields.Str(default=generate_date(), missing=generate_date())
    updated_date = fields.Str(default=generate_date(), missing=generate_date())
    active = fields.Str(default=True, missing=True)

    class Meta:
        unknown = EXCLUDE
