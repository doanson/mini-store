from mini_store.models.order import Order
from mini_store.common.connect import connect_db
from mini_store.exceptions.errrors import CommonError
from bson.objectid import ObjectId
import json
from bson import json_util

db = connect_db('127.0.0.1', 27017, 'mini-store')

db_order = db['order']
db_dish = db['dish']
db_user = db['user']


def handle_create_order(data):
    new_order = Order().load(data)
    new_order.update({
        "_id": str(ObjectId()),
        "created_by": data.get("user_id")})
    result = db_order.insert_one(new_order)
    return result.inserted_id


def get_order_detail(data):
    id = data.get("id")

    if id is None:
        raise CommonError("OrderID is empty. Please check again!!!")

    order = db_order.find_one({"_id": id})

    if order is None:
        raise CommonError("Order not found. Please check again!!!")

    dish = order.get("dish")
    dish_details = []
    total_price = 0

    if len(dish):
        for obj in dish:
            dish_data = db_dish.find_one({"_id": obj.get("id")})
            if dish_data:
                dish_data.update({
                    "amount": obj.get("amount")
                })
                dish_details.append(dish_data)
                total_price += float(dish_data.get("price")) * int(obj.get("amount"))

        order.update({"dish": dish_details})

    customer_id = order.get("customer_id")
    customer_detail = db_user.find_one({"_id": customer_id})
    customer_name = customer_detail.get("first_name") + " " + customer_detail.get(
        "last_name") if customer_detail else None

    order.update({"customer_name": customer_name})
    order.update({"total_price": total_price})

    return order


def get_all_order(current_page, record_per_page, user_id=None):
    skip = (current_page - 1) * record_per_page
    limit = record_per_page

    match_stage = {
        "active": True
    }
    if user_id is not None:
        user = db_user.find_one({"_id": user_id})
        if user is None:
            return []
            # raise CommonError("User not exist")

        match_stage.update({
            "customer_id": user_id
        })

    result = db_order.aggregate([
        {
            "$match": match_stage
        },
        {
            "$unwind": {
                "path": "$dish",
                "preserveNullAndEmptyArrays": True
            }
        },
        {
            "$lookup": {
                "from": "dish",
                "localField": "dish.id",
                "foreignField": "_id",
                "as": "dis_detail"
            }
        },
        {
            "$unwind": {
                "path": "$dis_detail",
                "preserveNullAndEmptyArrays": True
            }
        },
        {
            "$addFields": {
                "price": {
                    "$multiply": ["$dish.amount", "$dis_detail.price"]
                }
            }
        },

        {
            "$group": {
                "_id": "$_id",
                "dishes": {
                    "$push": "$dis_detail"
                },
                "total_price": {"$sum": "$price"},
                "paid": {"$first": "$paid"},
                "name": {"$first": "$name"},
                "created_by": {"$first": "$created_by"},
                "updated_date": {"$first": "$updated_date"},
                "created_date": {"$first": "$created_date"},
                "customer_id": {"$first": "$customer_id"},

            }
        },
        {
            "$sort": {
                "_id": -1,
                "created_date": -1
            }
        },
        {
            "$facet": {
                "total_record": [
                    {"$count": "total"},

                ],
                "data": [{"$skip": skip}, {"$limit": limit}]
            }
        },
        {
            "$addFields": {
                "total_record": {
                    "$arrayElemAt": ["$total_record.total", 0]
                }
            }

        },
        {
            "$addFields": {
                "total_page": {
                    "$ceil": {"$divide": ["$total_record", "$total_record"]}

                }
            }
        }

    ])
    result = json.loads(json_util.dumps(result))
    if result[0] and result[0].get('data'):
        pass

    return result[0]


def handle_edit_order(id, data):
    new_order = Order(only=("dish", "name", "paid", "updated_date")).load(data)
    result = db_order.update_one({"_id": id}, {
        "$set": {
            **new_order

        }
    })
    return id


def handle_delete_order(id):
    db_order.update_one({"_id": id}, {
        "$set": {
            "active": False
        }
    })
    return id
