from bson.objectid import ObjectId
from mini_store.models.dish import Dish
from mini_store.common.connect import connect_db
from mini_store.exceptions.errrors import CommonError
import json
from bson import json_util


db = connect_db('127.0.0.1', 27017, 'mini-store')

db_dish = db['dish']
db_menu = db['menu']


def handle_create_dish(data):
    new_dish = Dish().load(data)
    new_dish.update({"_id": str(ObjectId()), "created_by": data.get("user_id")})
    result = db_dish.insert_one(new_dish)

    return result.inserted_id


def handle_update_dish(data):
    id = data.get("id")
    result = db_dish.find_one(({"_id": id}))

    if result is None:
        raise CommonError("Dish not found. Please check again!!!")

    dish = Dish(only=("name", "description", "price", "photos", "updated_date")).dump(data)

    db_dish.update_one({"_id": id}, {"$set": {
        "name": dish.get("name"),
        "description": dish.get("description"),
        "price": dish.get("price"),
    }})

    if len(dish.get("photos")):
        db_dish.update_one({
            "_id": id
        }, {
            "$addToSet": {
                "photos": {
                    "$each": dish.get("photos")
                }
            }
        })

    return id


def handle_delete_dish(id):
    db_dish.delete_one({"_id": id})
    db_menu.update_many({}, {
        "$pull": {
            "dish": id
        }
    },
                   )
    return id


def get_dish(id):
    result = db_dish.find_one({"_id": id})
    return result


def get_all_dish(current_page, record_per_page):
    skip = (int(current_page) - 1) * record_per_page
    limit = int(record_per_page)


    result = db_dish.aggregate([
        {
            "$sort": {
                "_id": -1
            }
        },
        {
            "$facet": {
                "total_record": [
                    {"$count": "total"},

                ],
                "data": [{"$skip": skip}, {"$limit": limit}]
            }
        },
        {
            "$addFields": {
                "total_record": {
                    "$arrayElemAt": ["$total_record.total", 0]
                }
            }

        },
        {
            "$addFields": {
                "total_page": {
                    "$ceil": {"$divide": ["$total_record", "$total_record"]}

                }
            }
        }
    ])
    result = json.loads(json_util.dumps(result))
    if len(result):
        return result[0]
    return {}
