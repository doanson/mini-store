from mini_store.models.menu import Menu
from mini_store.common.connect import connect_db
from mini_store.exceptions.errrors import CommonError
from bson.objectid import ObjectId

# from mini_store.models.dish import Dish
import json
from bson import json_util

db = connect_db('127.0.0.1', 27017, 'mini-store')

db_menu = db['menu']
db_dish = db['dish']


def handle_create_menu(data):
    new_menu = Menu().load(data)
    new_menu.update({"_id": str(ObjectId()), "created_by": data.get("user_id")})
    result = db_menu.insert_one(new_menu)
    return result.inserted_id


def get_menu_details(id):
    if id is None:
        raise CommonError("ID is empty")

    result = db_menu.find_one({"_id": id, "active": True})

    if result is None:
        raise CommonError("Menu not found")

    dish = result.get("dish")
    dishes = []

    if len(dish):
        for id in dish:
            dish_detail = db_dish.find_one({"_id": id})
            if dish_detail:
                dishes.append(dish_detail)

        result.update({"dish": dishes})

    return result


def get_all_menu(current_page, record_per_page):
    skip = (int(current_page) - 1) * record_per_page
    limit = int(record_per_page)

    result = db_menu.aggregate([
        {
            "$match": {
                "active": True
            }},
        {
            "$unwind": {
                "path": "$dish",
                "preserveNullAndEmptyArrays": True
            }
        },
        {"$lookup": {
            "from": "dish",
            "localField": "dish",
            "foreignField": "_id",
            "as": "dis_detail"
        }},
        {
            "$unwind": {
                "path": "$dis_detail",
                "preserveNullAndEmptyArrays": True
            }
        },
        {
            "$group": {
                "_id": "$_id",
                "data": {
                    "$push": "$dis_detail"
                },
                "name": {"$first": "$name"},
                "description": {"$first": "$description"},
                "updated_date": {"$first": "$updated_date"},
                "created_date": {"$first": "$created_date"}

            }
        },
        {
          "$sort": {
              "_id": -1
          }
        },
        {
            "$facet": {
                "total_record": [
                    {"$count": "total"},

                ],
                "data": [{"$skip": skip}, {"$limit": limit}]
            }
        },
        {
            "$addFields": {
                "total_record": {
                    "$arrayElemAt": ["$total_record.total", 0]
                }
            }

        },
        {
            "$addFields": {
                "total_page": {
                    "$ceil": {"$divide": ["$total_record", "$total_record"]}

                }
            }
        }
    ])
    result = json.loads(json_util.dumps(result))
    if len(result):
        return result[0]
    return {}


def handle_delete_menu(id):
    result = db_menu.update_one({
        "_id": id
    }, {
        "$set": {
            "active": False
        }
    })
    return id


def update_menu(id, data):
    menu = Menu(only=("name", "dish", "description", "photos", "updated_date")).load(data)

    db_menu.update_one({"_id": id}, {
        "$set": {
            **menu
        }
    })
    return id
