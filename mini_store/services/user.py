from bson.objectid import ObjectId
from mini_store.models.user import User
from mini_store.common.token import encode_token
from mini_store.exceptions.errrors import CommonError
from mini_store.common.connect import connect_db
from mini_store.common.bycrypt import get_hashed_password, check_password
from mini_store.common.utils import create_directory
from mini_store.common.bycrypt import get_hashed_password
from cv2 import IMREAD_UNCHANGED, imdecode, imwrite
import numpy
import os
import json
from bson import json_util

db = connect_db('127.0.0.1', 27017, 'mini-store')

db_user = db['user']
db_dish = db['dish']


def handle_register(data):
    # Get data from request_body
    user = User().load(data)

    check_exist = db_user.find_one({"username": user.get("username")})

    if check_exist is not None:
        raise CommonError("Username existed!!!")

    user.update({"password": get_hashed_password(user.get("password"))})
    user.update({"_id": str(ObjectId())})

    result = db_user.insert_one(user)
    return str(result.inserted_id)


def handle_update_user(data):
    id = data.get("id")
    if id != data.get("user_id"):
        raise CommonError("You can not edit this user")

    if id is None:
        raise CommonError("user _id is empty")

    del data['id']

    user = User(only=("first_name", "last_name", "height", "weight", "email")).load(data)
    db_user.update_one({"_id": id}, {"$set": {
        **user
    }})
    return id


def handle_delete_user(data, role, user_id):
    admin = db_user.find_one({"_id": user_id})
    if admin is not None and admin.get("role") == "admin" or user_id == data.get("_id"):
        db_user.update_one({"_id": data.get("_id")}, {"$set": {
            "active": False
        }})
        return data.get("_id")
    raise CommonError("You must be admin or account owner to deactivate this account")


def validate_user(data):
    user = User(only=("username", "password")).load(data)

    result = db_user.find_one({"username": user.get("username")})

    if result is None:
        raise CommonError("User not found!!")

    plain_password = user.get("password")
    hashed_password = get_hashed_password(plain_password)
    save_password = result.get("password")

    if check_password(plain_password, save_password):
        exp = 24
        if data.get("remember"):
            exp = 30 * 24
        token = encode_token(result, exp)

        result.update({"token": token})
        del result["password"]

    else:
        raise CommonError("Wrong password!!!")

    return result


def upload_image_to_folder(files, folder_name):
    path_obj = create_directory(folder_name)
    os.chdir(path_obj.get("child_path"))
    # absolute_path = os.abspath()
    image_link = []

    for file in files:
        name = file.name
        filestr = file.read()
        file_path = os.path.abspath(f'{name}')
        # file_path = os.path.abspath(file_path)
        # print("etes", )
        print("file path: ", file_path)
        npimg = numpy.fromstring(filestr, numpy.uint8)
        image_link.append(file_path)

        img = imdecode(npimg, IMREAD_UNCHANGED)
        imwrite(file.name, img)

    os.chdir(path_obj.get("parent_path"))
    return image_link


def upload_dish_image(file, data, dish_id, folder_name="dish"):
    image_link = upload_image_to_folder(file, folder_name)
    return image_link


def upload_menu_image(file, data, dish_id, folder_name="menu"):
    image_link = upload_image_to_folder(file, folder_name)

    return image_link


def upload_menu_image(file, folder_name="order"):
    image_link = upload_image_to_folder(file, folder_name)

    return image_link


def get_user_info(id):
    user = db_user.find_one({"_id": id, "active": True}, {"password": 0})
    return user


def get_all_user(current_page, record_per_page=5):
    skip = (current_page - 1) * record_per_page
    limit = record_per_page
    result = db_user.aggregate([
        {
            "$match": {
                "role": "user",
                "active": True

            }},
        {
            "$facet": {
                "total_record": [
                    {"$count": "total"},

                ],
                "data": [{"$skip": skip}, {"$limit": limit}]
            }
        },
        {
            "$addFields": {
                "total_record": {
                    "$arrayElemAt": ["$total_record.total", 0]
                }
            }

        },
        {
            "$addFields": {
                "total_page": {
                    "$ceil": {"$divide": ["$total_record", "$total_record"]}

                }
            }
        }
    ])
    result = json.loads(json_util.dumps(result))
    return result


def update_password(data):
    old_password = data.get("old_password")
    new_password = data.get("new_password")
    user_id = data.get("user_id")

    if old_password is None:
        raise CommonError("Old password is empty")
    if new_password is None:
        raise CommonError("New password is empty")

    # hash_password = get_hashed_password(old_password)

    user = db_user.find_one({"_id": user_id})
    if user is None:
        raise CommonError("user not exist")

    user_password = user.get("password")
    if not check_password(old_password, user_password):
        raise CommonError("Password is not correct")

    new_hashed_password = get_hashed_password(new_password)
    db_user.update_one({"_id": user_id}, {
        "$set": {
            "password": new_hashed_password
        }
    })



