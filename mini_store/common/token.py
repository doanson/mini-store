import token
import datetime
import jwt
import json
from bson.json_util import dumps
from mini_store.common.utils import handle_error, handle_success
from functools import wraps
from mini_store.common.connect import connect_db

TOKEN_SECRET_KEY = 'Secret_Key'
db = connect_db('127.0.0.1', 27017, 'mini-store')
db_user = db['user']

def encode_token(data, exp):
    time_limit = datetime.datetime.utcnow() + datetime.timedelta(hours=exp)
    payload = {
        "exp": time_limit,
        "_id": data.get("_id"),
    }
    token = jwt.encode(payload, TOKEN_SECRET_KEY,
                       algorithm='HS256').decode("utf-8")

    return token


def decode_token(request):
    try:
        token = request.headers.get("Authorization")

        if token != "":

            token = token.replace("Bearer ", "")
            try:
                decoded_token = jwt.decode(token, TOKEN_SECRET_KEY,
                                           algorithms="HS256")
                return decoded_token
            except jwt.exceptions.ExpiredSignatureError:
                return handle_error("Token has expired")
            except Exception as e:
                return handle_error(e)
        else:
            return handle_error("Token expired")
    except Exception as e:
        return handle_error("Token is empty")


def validate_token(func):
    @wraps(func)
    def wrap(request, *args, **kwargs):
        try:
            token = request.headers.get("Authorization")

            if token != "":

                token = token.replace("Bearer ", "")
                try:
                    decoded_token = jwt.decode(token, TOKEN_SECRET_KEY,
                                               algorithms="HS256")
                    content_type = request.headers['content-type']

                    if content_type == 'application/json':
                        request.body = json.loads(request.body)
                        request.body.update({
                            "user_id": decoded_token.get("_id"),
                            # "role": decoded_token.get("role"),
                            "user_info": decoded_token
                        })
                    else:
                        user = {
                            "user_id": decoded_token.get("id"),
                            "role": decoded_token.get("role")
                        }
                        request.user = user
                    return func(request, *args, **kwargs)
                except jwt.exceptions.ExpiredSignatureError:
                    return handle_error("Token has expired")
                except Exception as e:
                    return handle_error(e)
            else:
                return handle_error("Token expired")

        except Exception as e:
            return handle_error("Token is empty")

    return wrap


def check_role_admin(f):
    def wrap(request, *args, **kwargs):
        id = request.body.get("user_id")

        if id is None:
            return handle_error("User id in  token not valid")
        user = db_user.find_one({"_id": id})
        if user is None:
            return handle_error("User not exist")
        if user.get("role") == "admin":
            return f(request, *args, **kwargs)
        else:
            return handle_error("User do not have admin role")

    return wrap
