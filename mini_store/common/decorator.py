import pprint
from functools import wraps
from utils import handle_error
from token import decode

def admin_only(f):
    def wrap(request, *args, **kwargs):
        return f(request, *args, **kwargs)

    return wrap