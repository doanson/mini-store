# Generate connection to mongodb

from mini_store.common.utils import handle_error
from pymongo import MongoClient


def connect_db(host='127.0.0.1', port=27017, db_name='mini-store'):
    try:
        client = MongoClient(host=host, port=int(port))
        print("Connect to the db success!!!")

        return client[db_name]
    except Exception as e:
        return handle_error(e)
    