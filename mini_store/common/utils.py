import time

from django.http import JsonResponse
from rest_framework import status
import os
from mini_store.exceptions.errrors import CommonError
import datetime
import pytz


def handle_error(error, message="UNKNOWN ERROR", status_code=status.HTTP_500_INTERNAL_SERVER_ERROR):
    if isinstance(error, CommonError):
        message = error.message or message
        status_code = error.status_code or status_code
        return JsonResponse({"error": str(message), "status_code": status_code}, status=status_code)

    return JsonResponse({"error": str(error), "status_code": status_code}, status=status_code)


def handle_success(data, status_code=status.HTTP_200_OK):
    return JsonResponse({"data": data, "status_code": status_code, "message": "success"}, status=status_code)


def create_directory(name="image"):
    current_cwd = os.getcwd()
    parent_path = f'{current_cwd}\\mini_store\\images'
    child_path = f'{parent_path}\\{name}'

    if os.path.exists(child_path):
        pass
    else:
        os.mkdir(child_path)
    return {
        "parent_path": current_cwd,
        "child_path": child_path
    }


def generate_date():
    tz = pytz.timezone('Asia/Ho_Chi_Minh')
    time_now = datetime.datetime.now(datetime.timezone.utc).astimezone(tz)
    millis = int(time.mktime(time_now.timetuple()))
    dat = datetime.datetime.fromtimestamp(millis).strftime('%Y-%m-%d %H:%M:%S')

    return dat
