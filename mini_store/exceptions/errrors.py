from rest_framework.exceptions import APIException
from rest_framework import status


class Error(Exception):
    pass


class CommonError(Error):
    def __init__(self, message='INTERNAL SERVER ERROR', status_code=status.HTTP_500_INTERNAL_SERVER_ERROR):
        self.message = message
        self.status_code = status_code
        super(CommonError, self).__init__(message)
